# Installation instructions

## Install on DEBIAN

If you are using DEBIAN then you can run the following utility:

 - debian/rules binary
 - dpkg -i <packagename>

## Install on EL8 and clones

On [RHEL8](https://en.wikipedia.org/wiki/Red_Hat_Enterprise_Linux) and EL8 clones such as [RockyLinux](https://rockylinux.org/) and [AlmaLinux](https://almalinux.org/).  There 
is a `pxeconfig.spec` file  but that needs someone that will update it 
as a the debian package.

### Manual build

you have to install and build the software manually:
 * Install prerequisite software (later *Python* versions such as `python39` may also be installed):
```
dnf install python36 autoconf make gcc
dnf install telnet telnet-server xinetd tftp-server tftp syslinux syslinux-tftpboot grub2-efi-x64
```

 * Download the software:
```
wget https://gitlab.com/surfsara/pxeconfig/-/archive/master/pxeconfig-master.tar.gz
tar xzf pxeconfig-master.tar.gz
```

 * Configure to install software into `/usr/local/`:
```
cd pxeconfig-master
autoconf
./configure --prefix=/usr/local
```

Build and install the software into the `/` top-level destination directory:
```
make install DESTDIR=/
```

#### Configure the pxeconfigd service 

This is not needed if the debian package has been installed.  To activate 
the `pxeconfigd` service use this procedure:

1) Add a port `6611/tcp` to `/etc/services`, for example:
```
cat <<EOF >>/etc/services
pxeconfigd      6611/tcp     # pxe config daemon
EOF
```
You must open port `6611/tcp` in the firewall:
```
firewall-cmd --permanent --zone=public --add-port=6611/tcp
firewall-cmd --reload
```

2) Install the `xinetd` or the `systemd` service:
```
cp examples/pxeconfigd.xinetd /etc/xinetd.d/pxeconfigd
systemctl enable xinetd
systemctl restart xinetd
```

3) Create the PXE boot directory:
```
mkdir -pv /tftpboot/pxelinux.cfg
mkdir -pv /tftpboot/uefi
```
Note: The `/tftpboot` is a soft-link to `/var/lib/tftpboot` (installed by the `tftp-server` package).

4) You can now test `pxeconfigd` by connecting from the server localhost IPv4-address:
```
$ telnet 127.0.0.1 pxeconfigd 
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
ip = 127.0.0.1
pxe_file = /var/lib/tftpboot/pxelinux.cfg/7F000001, uefi_file = /var/lib/tftpboot/uefi/grub.cfg-7F000001
Connection closed by foreign host.
```
As you can see `/var/lib/tftpboot/pxelinux.cfg/` is the default directory,
and there is also an UEFI PXE folder.

In syslog a line will be added if the removal was succesful:
```
Mar 20 14:12:07 <server-name> xinetd[480353]: START: pxeconfigd pid=480377 from=::ffff:127.0.0.1
```
You may also test the above telnet command from another host to make sure that the firewall has been opened correctly:
```
other-host$ telnet <server-IP-address> 6611
```

**Note:** The `pxeconfigd` only works with IPv4 addresses, not IPv6!

## The pxeconfig utility

The `pxeconfig` utility can handle UEFI or BIOS setup. The default for now is
BIOS.  The filenames and directory are different for:
 * BIOS --> boot_dir = /tftpboot/pxelinux.cfg --> prefix = `default.`
 * UEFI --> boot_dir = /tftpboot/uefi --> prefix = `default.`

If the daemon works and you want to install/reinstall a node, a link
must be placed in the `boot_dir` directory. `boot_dir` is different for UEFI or BIOS. 
This can be done with the `pxeconfig` utility.  This utility will ask some questions and will 
make the links for you.  The utility requires the following setup:
>
> 1) All the PXE configuration files must start with the keyword `default.`
>  N.B.: yes, the dot after `default` variable is part and parcel of the keyword!
>
> 2) If default is a symbolic link to for example `default.localboot`, then
>  `default.localhost` is not included in the list where the user can choose from.
>

In this distribution examples are included for bios and uefi booting. Pxeconfig 
lets the user decide which `default.` config file to use.  

### Usage 

On the PXE image server setup client PXE boot installation, for example:
```
pxeconfig <client-DNS-name>
```
You can also give a command line switch to specify where the pxeconfig files reside:
```
pxeconfig -d /<some>/<other>/<place> <client-DNS-name>
```

## Network installation of a node

The automated PXE installation of a client should conclude by **removing**
the ```/tftpboot/boot_dir/prefix-<hex-address>``` file on the PXE server (denoted as $IMAGESERVER).

To activate this on a client you must add the following lines to the `master` installation 
script of the node:
```
IMAGESERVER=<server-IP-address>
telnet $IMAGESERVER 6611
```
>
> (`IMAGESERVER` is the IP-address of the PXE bootserver.)
>
The above `telnet` connection will remove the PXE config file for the node from the PXE bootserver.
It may be a good idea place the command just before the last commands in PXE installation scripts.
It will look into the UEFI and BIOS directory for the removal of the file.

**Note:** Make sure that the `telnet` package gets installed in the client during the automated 
installation.

## The hexls utility

The last utility is `hexls`.
This utility will display the hex PXE config file with the corresponding ip-addresses.

Comments or Suggestions 
---------------------------

See https://gitlab.com/surfsara/pxeconfig/-/issues and https://www.surf.nl for more info about SURF
