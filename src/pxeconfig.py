# set ts=4, sw=4
#
# Author: Bas van der Vlies <bas.vandervlies@surf.nl>
# Date  : 16 February 2002
#
# Tester: Walter de Jong <walter.dejong@surf.nl>
#
# Copyright (C) 2021
#
# This file is part of the pxeconfig utils
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
#
"""
Usage: pxeconfig [-f,--filename <name>] <hosts|mac_addresses>

Specifying hosts or mac addresses:
   To specify a range use the [] to indicate a range,
   a couple of examples:

   The first five nodes of rack 16
    - gb-r16n[1-5]

   The first five nodes and node 12 and 18 of rack 16 to 20
      - gb-r[16-20]n[1-5,12,18]

   The first five nodes de in rack 16 with padding enabled
    - gb-r[16]n[01-5]

   Host with mac address 00:19:b9:de:21:47 and first five node of rack 15
    - 00:19:b9:de:21:47 gb-r15n[1-5]

The ranges ([]) are not only limited to numbers, letters can also be used.

With this program you can configure which PXE configuration file a node
will use when it boots.

See following link for usage and examples:
 - https://gitlab.com/surfsara/pxeconfig/-/wikis/usage
"""

import sys, os
import glob
import getopt
import socket
#import ConfigParser
import re

# import from the sara_python_modules
import AdvancedParser
from pxe_global import *

# Constants
#
NETWORK       = 'network'
START         = 'start'
END           = 'end'
VERSION       = '5.1.2'

UEFI_CONF = {
        "dir" : "/tftpboot/uefi",
        "default_boot_file": "grub.cfg",
        "menu_files_regex": "default.*",
        "boot_file_format": "grub.cfg-"
}

BIOS_CONF = {
        "dir" : "/tftpboot/pxelinux.cfg",
        "default_boot_file": "default",
        "menu_files_regex": "default.*",
        "boot_file_format": ""
}
BOOT_METHODS = {
        "bios" : BIOS_CONF,
        "uefi" : UEFI_CONF
}

def select_configfile(opts):
    """
    Let user choose which pxeconfig file to use.
    """

    os.chdir(BOOT_METHODS[opts.boot_method]['dir'])

    # Try to determine to which file the default symlink points, and
    # if it exists, remove it from the list.
    #
    try:
        default_boot_file = os.readlink(BOOT_METHODS[opts.boot_method]['default_boot_file'])
    except OSError:
        default_boot_file = None
        pass

    files = glob.glob(BOOT_METHODS[opts.boot_method]['menu_files_regex'])
    if not files:
        error =  'There are no boot configuration files starting with: {}'.format(BOOT_METHODS[opts.boot_method]['menu_files_regex'])
        raise PxeConfig(error)

    if default_boot_file:
        files.remove(default_boot_file)

    # sort the files
    #
    files.sort()

    print('Which pxe config file must we use: ?')
    i = 1
    for file in files:
        print("{:d} : {}".format(i,file))
        i = i + 1

    while 1:
        index = input('Select a number: ')
        try:
            index = int(index)
        except ValueError:
            index = len(files) + 1

        # Is the user smart enough to select
        # the right value??
        #
        if 0 < index <= len(files):
            break

    return files[index-1]


def manage_links(opts, haddr, ip_addr):
    """
    Create the links in the CONF_DIR,
    list : A list containing: network hex address, config file,
           start number, end number
    """

    if opts.VERBOSE:
        print(f"manage_links({haddr})")

    boot_file = f"{BOOT_METHODS[opts.boot_method]['boot_file_format']}{haddr}"
    print(boot_file)

    boot_dir = BOOT_METHODS[opts.boot_method]['dir']
    os.chdir(boot_dir)

    if opts.REMOVE:
        if opts.DEBUG or opts.DRY_RUN or opts.VERBOSE:
            print(f"removing {boot_dir}/{boot_file}")

            if opts.SCRIPT_HOOK_REMOVE and ip_addr:
                print(f"Executing client script hook remove : {opts.SCRIPT_HOOK_REMOVE} with arg: {ip_addr}")

        if not opts.DRY_RUN:
            if os.path.exists(boot_file):
                os.unlink(boot_file)

            if opts.SCRIPT_HOOK_REMOVE and ip_addr:
                cmd = f"{opts.SCRIPT_HOOK_REMOVE} {ip_addr}"
                run_command(opts, cmd)

    else:

        if opts.DEBUG or opts.DRY_RUN or opts.VERBOSE:
            print(f"linking {boot_dir}/{boot_file} to {boot_dir}/{opts.filename}")

            if opts.SCRIPT_HOOK_ADD and ip_addr:
                print(f"Executing client script hook add : {opts.SCRIPT_HOOK_ADD} with arg: {ip_addr}")

        if not opts.DRY_RUN:
            if os.path.exists(boot_file):
                os.unlink(boot_file)
            os.symlink(opts.filename, boot_file)

            if opts.SCRIPT_HOOK_ADD and ip_addr:
                cmd = f"{opts.SCRIPT_HOOK_ADD} {ip_addr}"
                run_command(opts, cmd)

def net_2_hex(net, opts):
    """
    This function checks if the give network is a Class C-network and will
    convert the network address to a hex address if true.
    """
    if opts.DEBUG:
        print(f"net_2_hex : {net}")

    d = net.split('.')
    if len(d) != 3:
        error = f"{net} is not a valid  C-class network address"
        raise PxeConfig(error)

    # Check if we have valid network values
    r = ''
    for i in d:
        r = f"{r}{int(i):02X}"

    if opts.DEBUG:
        print(f"C-network converted to hex: {r}")

    return r

def host_2_hex(host, opts):
    """
    Convert hostname(s) to a net address that can be handled by manage_links function
    """
    if opts.DEBUG:
        print(f"host_2_hex: {host}")

    try:
        addr = socket.gethostbyname(host)
    except socket.error as detail:
        error =  f"{host} not an valid hostname: {detail}"
        raise PxeConfig(error)

    net = addr.split('.')
    cnet = '.'.join(net[0:3])


    haddr = '{}{:02X}'.format(net_2_hex(cnet, opts), int(net[3]))
    manage_links(opts, haddr, addr)


def mac_2_hex(mac_addr, opts):
    """
    Convert mac address to pxeconfig address
    """
    if opts.DEBUG:
        print(f"mac_2_hex: {mac_addr}")

    haddr = '01-{}'.format(mac_addr.replace(':', '-').lower())
    manage_links(opts, haddr, None)

def add_options(p):
    """
    add the default opts
    """
    p.set_defaults(
        DEBUG   = False,
        DRY_RUN = False,
        REMOVE  = False,
        SCRIPT_HOOK_ADD = False,
        SCRIPT_HOOK_REMOVE = False,
        SKIP_HOSTNAME_ERRORS = False,
        UEFI_MODE = False,
        VERBOSE = False,
        VERSION  = False,
    )

    p.add_option('-b', '--boot-method',
        action = 'store',
        dest   = 'boot_method',
        choices=['bios', 'uefi'],
        help   = 'which boot method do we use: (default: bios for now)'
    )

    p.add_option('-d', '--debug',
        action = 'store_true',
        dest   = 'DEBUG',
        help   = 'Toggle debug mode (default : False)'
    )

    p.add_option('-f', '--filename',
        action = 'store',
        dest   = 'filename',
        help   = 'Specifies which PXE filename must be use'
    )

    p.add_option('-n', '--dry-run',
        action = 'store_true',
        dest   = 'DRY_RUN',
        help   = 'Do not execute any command (default : False)'
    )

    p.add_option('-r', '--remove',
        action = 'store_true',
        dest   = 'REMOVE',
        help   = 'Removes the PXE filename for a host(s)'
    )

    p.add_option('-u', '--uefi',
        action = 'store_true',
        dest   = 'UEFI_MODE',
        help   = 'switch to UEFI mode setup (default: False)'
    )

    p.add_option('-v', '--verbose',
        action = 'store_true',
        dest   = 'VERBOSE',
        help   = 'Make the program more verbose (default: False)'
    )

    p.add_option('-H', '--skip-hostname-lookup-error',
        action = 'store_true',
        dest   = 'SKIP_HOSTNAME_LOOKUP_ERROR',
        help   = 'When hostname lookup fails, skip it (default: False)'
    )

    p.add_option('-V', '--version',
        action = 'store_true',
        dest   = 'VERSION',
        help   = 'Print the program version number and exit'
    )

def run(hosts, opts, f_config):
    """
    Make use of sara advance parser module. It can handle regex in hostnames
    """
    # Only check if we have specified an pxeconfig file if we did not
    # specify the REMOVE option
    #
    if not opts.REMOVE:
        if opts.filename:
            if not os.path.isfile(os.path.join(BOOT_METHODS[opts.boot_method]['dir'], opts.filename)):
                error =  f"{opts.filename}: Filename does not exist"
                raise PxeConfig(error)
        else:
            opts.filename = select_configfile(opts)

    # Are the hosts wiht only mac addresses defined in the configuration file
    # or specified on the command line
    #
    mac_addr_re = re.compile('([a-fA-F0-9]{2}[:|\-]?){6}')

    for host in hosts:
        if host in f_config.sections():
            mac_addr = f_config.get(host, 'mac_address')
            mac_2_hex(mac_addr, opts)

        elif mac_addr_re.search(host):
            mac_2_hex(host, opts)
        else:
            try:
                host_2_hex(host, opts)
            except PxeConfig as detail:

                if opts.SKIP_HOSTNAME_LOOKUP_ERROR:
                    print(f"Skipping hostname lookup failed for: {host}")
                    continue
                else:
                    print(detail)
                    sys.exit(1)

def set_and_check_options(opts, file_defaults):
    """
    This  will set some global variables and check if dirs exists
    """
    global BIOS_CONFIG
    global UEFI_CONFIG

    # debug can be specified by the command line or opts file
    #
    if not opts.DEBUG:
        try:
            if file_defaults['debug']:
                opts.DEBUG = int(file_defaults['debug'])
        except KeyError:
            pass

    try:
        BIOS_CONFIG['dir'] = file_defaults['pxe_config_dir']
    except KeyError:
        pass

    try:
        BIOS_CONFIG['menu_files_regex'] = file_defaults['bios_menu_files_regex']
    except KeyError:
        pass

    try:
        UEFI_CONFIG['dir'] = file_defaults['uefi_config_dir']
    except KeyError:
        pass

    try:
        UEFI_CONFIG['menu_files_regex'] = file_defaults['uefi_menu_files_regex']
    except KeyError:
        pass

    try:
        file_boot_method = file_defaults['boot_method']
        file_boot_method = file_boot_method.lower()
    except KeyError:
        file_boot_method = 'bios'
        pass


    if opts.UEFI_MODE:
        print("\n NOTE: Obsolete option use: '--b|--boot-method uefi` \n")
        opts.boot_method = "uefi"

    ## commmand line option wins
    if not opts.boot_method:
        opts.boot_method = file_boot_method

    ## check dirs for all boot methods
    for b in BOOT_METHODS:
        BOOT_METHODS[b]['dir'] = os.path.realpath(BOOT_METHODS[b]['dir'])
        if not os.path.isdir(BOOT_METHODS[b]['dir']):
            error =  f"{b} directory: {BOOT_METHODS[b]['dir']} does not exist"
            raise PxeConfig(error)

    try:
        opts.SCRIPT_HOOK_ADD = file_defaults['client_script_hook_add']
    except KeyError as detail:
        pass

    try:
        opts.SCRIPT_HOOK_REMOVE = file_defaults['client_script_hook_remove']
    except KeyError as detail:
        pass

    if not opts.SKIP_HOSTNAME_LOOKUP_ERROR:
        try:
            opts.SKIP_HOSTNAME_LOOKUP_ERROR = file_defaults['skip_hostname_lookup_error']
        except KeyError as detail:
            pass

def main():
    parser = AdvancedParser.AdvancedParser(usage=__doc__)
    add_options(parser)
    opts, args = parser.parse_args()

    if opts.VERSION:
        print(VERSION)
        sys.exit(0)

    if not args:
        parser.print_help()
        sys.exit(0)

    if opts.DEBUG:
        print(args, opts)

    file_config, file_default_section = ReadConfig()

    set_and_check_options(opts, file_default_section)
    run(args, opts, file_config)

if __name__ == '__main__':
    try:
        main()
    except PxeConfig as detail:
        print(detail)
        sys.exit(1)
