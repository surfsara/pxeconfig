<!-- vim-markdown-toc GFM -->

* [5.1.2](#512)
* [5.1.1](#511)
* [5.1.0](#510)
* [5.0.0](#500)
* [4.2.X](#42x)
* [4.2.0](#420)
* [4.1.0:](#410)
* [4.0.3:](#403)
* [4.0.2:](#402)
* [3.1.2:](#312)
* [3.1.0:](#310)
* [3.0.4:](#304)
* [3.0.3:](#303)
* [3.0.2:](#302)
* [3.0.1:](#301)
* [3.0.0:](#300)
* [2.0.0](#200)
* [1.0.0](#100)
* [0.7](#07)
* [0.6.6](#066)
* [0.6.5](#065)
* [0.6.4:](#064)
* [0.6.3:](#063)
* [0.6.2:](#062)
* [0.6.1:](#061)
* [0.6:](#06)
* [0.5](#05)
* [0.4.4](#044)
* [0.4.3](#043)
* [0.4.2](#042)
* [0.4](#04)
* [0.3](#03)
* [0.2.1](#021)

<!-- vim-markdown-toc -->
# 5.1.2
  * Author: Bas van der Vlies
    * -f/--file option did not work

# 5.1.1
  * Author: Bas van der Vlies
    * Must use python formatted string for debug output

# 5.1.0
  * Author: Bas van der Vlies
    * fixed reading `default_boot_file` link
    * added new configuration option  `boot_method`, default bios
    * added new configuration option  `bios_menu_files_regex`, default `default.*`
    * added new configuration option  `uefi_menu_files_regex`, default `default.*`
    * added new configuration option  `uefi_config_dir`, default `/tftpboot/uefi`
    * added new command line option `-b|--boot-method <bios|uefi>`
    * rewrote a lot so we can easier switch between boot methods and others
    * added `grub.cfg` examples
    * rewrote debian package management we now use systemd pxeconfig service
    * include the systemd files for pxeconfigd in the examples directory
    * split the examples in `bios` and `uefi` directories

# 5.0.0
  * Author: Bas van der Vlies
    * UEFI support added for all utilities requested by Ole Holm Nielsen
    * Removed SVN tags
    * updated email addresses to surf

  * Author: Gwen Dawes
    * Code has beem ported from python 2 to 3
    * Restructured a lot of code
    * Added an systemd example for pxeconfigd

  * Author: Dennis Stam
    * replaced system() by subprocess code
    * replaced tabs by spaces
    * activated the hook remove script for the daemon

# 4.2.X
  * hexls output must be consistent sperated by '=>', useful for utilities like awk (Bas van der Vlies)

# 4.2.0
  * Added a new option for commandline and pxeconfig.conf:
    * commandline: `-H, --skip-hostname-lookup-error` When hostname lookup fails, skip it (default: False)
    * pxeconfig.conf: `skip_hostname_lookup_error: 1`
    * This is mostly used for cluster when you just want to install all the nodes, but not all racks/blade-chassis are equally filled with nodes. (Bas van der Vlies)

# 4.1.0:
  * Added two new configuration options for pxeconfig.conf file ( Bas van der Vlies)
```
     - client_script_hook_add    : The script that must be run when we create
                                 a pxeconfig file for a host(s). This option
                                 replaces client_script_hook.
     - client_script_hook_remove : The script that must be run wehn we remove
                                 a pxeconfig file for a host(s)

    Both options are useful for firewall setups. To open/close ports.
```

# 4.0.3:
  * Removed `server_args` from `pxeconfig.xinetd` example. Everything is set via the pxeconfig.conf file

# 4.0.2:
  * Improved error handeling for AdvanceParser.py (Dennis Stam)
  * debian/postinst. Use /bin/echo instead of echo else wrong entry in /etc/services (Bodo MeiBner)
  * debian/control. Fixed dependecies problems for pxeconfig package. Added inetd | xinetd (Bodo MeiBner)
  * pxemenu has been updated. See: (Ole Holm Nielsen)
     * INSTALL-pxemenu
     * README-pxemenu
     * Updates for SYSLINUX version 4.02.
     * Some obsolete menus (UBC,Ghost) have been removed.
  * Added support for hosts that only can use mac address for pxelinux configuration. This is used in environments where dynamic ip's are used (Bas van der Vlies)
  * Added post install script 98all.pxeconfig. It can be used in sali and systemimager (Ole Holm Nielsen)

# 3.1.2:
  * Added rpm spec file (Ramon Bastiaans)
  * Fixed a bug in pxconfig when `client_script_hook` is not set in pxeconfig file (Bas van der Vlies)
  * Removed some obselete code from pxeconfigd (Bas van der Vlies)

# 3.1.0:
  * Added client/daemon hook script code. Two new options for the pxeconfig.conf file: (Bas van der Vlies/Walter de Jong)
    * `daemon_script_hook`
    * `client_script_hook`
```
    We use these scripts to open (client_script_hook) and close
    (daemon_script_hook) ports on the firewall. So not everybody can do
    an rsync or use a bittorrent client to get useful info.
```

  * new module `pxe_global.py`, used by pxeconfig and pxeconfigd
  * Rewrite of the daemon code. removed the option -d/--directory is replaced by configuration file option.


# 3.0.4:
  * Fixed an error with importing pxeconfig if it is not in the standard python path. (Bas van der Vlies)

# 3.0.3:
  * --version/-V  prints now the version info and exits

# 3.0.2:
  * If option remove is specified, then do not ask for configuration file. (Bas van der Vlies)

# 3.0.1:
  * Import AdvancedParser instead of sara location.(Maarten van Ingen)

# 3.0.0:
  * Added the AdvancedParser module created by Dennis Stam (SARA). This removed a lot of obsolete code and command line options. The new version is not compatible with the previous one. (Bas van der Vlies & Dennis Stam)
  * New directory structure, added src directory. (Bas van der Vlies)
  * Pxeconfig is now a python module and can be used inside python programs. (Bas van der Vlies)
  * Added python setup method to support the new behaviour

# 2.0.0
  * Fixed a bug in -r/--remove option. Do not display pxe filemame menu. (Bas van der Vlies)
  * pxeconfigd.xinetd in the example directory is created by configure. (Bas van der Vlies & Ole Holm Nielsen)
  * Fixed some spell errors (Bas van der Vlies  & Ole Holm Nielsen)
  * Add new optione -w/--equal-width, eg:
```
    * pxeconfig -w --basename gb-r40n -s 1 -e 2 --filename default.install

   Will create links to default.install for hosts:
   	gb-r40n09
	gb-r40n10
```
  * Various bugs fixed with setting pxeconfig directory in pxeconfig.conf: (Bas van der Vlies)
    * Setting of the pxeconfig directory in config file did not have any effect
    * Convert pxeconfig directory to realpath else os.chdir fails
  * Fixed a bug in short option `-b` (basename) must have an argument. (Bas van der Vlies)
  * In analogue with systemimager we have a `-H,--host-range <number>-<number>` option. It must be used in combinatiion with `-b,--basenane`, eg:
```
     pxeconfig --basename gb-r1n --host-range 1-2 -filename default.memtestZ

   will produce links to file default.memtest for these hosts
      - gb-r1n1
      - gb-r1n2

  If leading zeros are used, then host names will be padded with zeros.
     pxeconfig --basename gb-r1n --host-range 01-02 -filename default.memtestZ

  will produce links to file default.memtest for these hosts
      - gb-r1n01
      - gb-r1n02

   Suggested by : Ole Holm Nielsen
   Implemented by : Bas van der Vlies
```

  * We do need the check for a C-compiler so remove it from the configure.in (Bas van der Vlies)
  * Added `-R,--rack` and `-N,--node` option, eg:
```
     pxeconfig --basename gb- --rack r1-r2 --node n1-n2 -f default.memtest

     will produce links to file defaul.memtest for these hosts:
       - gb-r1n1
       - gb-r1n2
       - gb-r2n1
       - gb-r2n2

  If leading zeros are used, then rack and/or node names will be padded with
  zeros, eg:
     pxeconfig --basename gb- --rack r01-r02 --node n01-n02 -f default.memtest

     will produce links to file defaul.memtest for these hosts:
       - gb-r01n01
       - gb-r01n02
       - gb-r02n01
       - gb-r02n02

   Author: Bas van der Vlies
```

 - Added `--dry-run` mode (Bas van der Vlies)

# 1.0.0
 * List of pxeconfig files are now sorted alphabetically
 * Use GNU getopt. This means that option and non-option arguments may be intermixed.
 * hexls now sorts its output
 * hexls now also display link info for hex files shorter dan 8 characters. Some hex files can point to a subnet IP adddress
 * Added `-b/--basename` option. This is for host with a common prefix, eg:
```
     pxeconfig --basename gb-r40n --start 1 --end 2 --filename default.install

   Will create links to default.install for hosts:
   	gb-r40n1
	gb-r40n2

   Implemented by: Bas van der Vlies
```

 * Added pxe-menu tools:
```
 The PXE-menu tools are used to control the booting of networked computers
 directly from the computer's console at the BIOS level before any operating
 system has been loaded.  This is extremely useful for diagnostic purposes,
 or for selecting an operating system installation method, of an individual
 computer.  For further information please read the files README-pxemenu and
 INSTALL-pxemenu.

   Implemented by: Ole Holm Nielsen
```

# 0.7
 * Added default.memtest and default.flashqla as example how to use pxeconfig utilities for other purposes then for systemimager.
 * Added configure script (autoconf tools).
 * Replaced the debian build package system by a better one that uses the added configure script. It must now be easy to build also a rpm package.
 * Added a configuraton file pxeconfig.conf, eg to specify the location of pxe config directory.
 * Removed the `-d/--directory` option for pxeconfig. It is replaced by the configuration file pxeconfig.conf (`pxe_config_dir`).

# 0.6.6
 * Fixed an error in postinst script for Debian.

# 0.6.5
 * added reading symlinks to hexls; this produces output like
```
    C0A811DD => 192.168.17.221 => gb-r13n5.irc.sara.nl -> default.node_install
```

# 0.6.4:
 * Fixed a bug in hexls with non resolving hostnames

# 0.6.3:
 * Added hostname lookup feature to hexls. If succes it will display the HEX address, ip-number, hostname, eg:
```
    C0A811DD => 192.168.17.221 => gb-r13n5.irc.sara.nl
```

# 0.6.2:
  * Fixed a bug in the non-interactive mode reported by and fixed by: (Charles Galpin)

# 0.6.1:
  * Interactive mode did not work anymore. Fixed it by adding `-i/--interactive` command line options.

# 0.6:
  - Added `-r|--remove` option. Now it is possible to remove the pxe boot file, eg:
```
    	pxeconfig --remove dummy.sara.nl
```

# 0.5
  * Added support for hostnames on the command line instead of network numbers, eg:
```
    	pxeconfig -f default.node_install bas.ka.sara.nl walter.ka.sara.nl
```

# 0.4.4
  * Updated documentation for xinetd support + example config
  * Added support for xinetd in the debian package scripts (Ramon Bastiaans)

# 0.4.3
  * Added `make_deb_pkg.sh` utility to make an DEBIAN package
  * changed port number to 6611 for pxeconfigd. Webmin is using the 10000 port number.

# 0.4.2
  * Minor change to pxeconfig/pxeconfigd utiltites. Added `-V/--version` flag for printing version info.
  * Updated the INSTALL instructions. Howto use it for systemimager 3.X versions

# 0.4
  * Pxeconfig can now be used interactively or via command line options.

# 0.3
  * Fixed a bug in hexls. The regular expression to determine if a filename is pxe hex filename or ordinary filename was wrong.

# 0.2.1
  * Bugfix now we can run pxeconfig a second time to change the pxe boot file.
  * Phrase change use 'Select a number:' instead of 'Choice a number'
