# Authors: Bas van der Vlies
# Date   : 28 Mar 2007
# Desc.  : Simple Makefile
#
prefix=@prefix@
exec_prefix=@exec_prefix@
SBIN_DIR=@sbindir@
BIN_DIR=@bindir@
DATA_DIR=@datadir@/doc/pxeconfig
CONFIG=@sysconfdir@

INSTALL=./install-sh

PYTHON_FILES=pxeconfig hexls
EXAMPLES=pxeconfigd.xinetd 98all.pxeconfig pxeconfigd@.service pxeconfigd.socket
EXAMPLES_DIR=bios uefi
PXEMENU_DIRS=pxemenu
PXEMENU_FILES=README-pxemenu INSTALL-pxemenu

all: compile

compile:
	@PYTHON@ setup.py build

install: install-conf install-examples install-pxemenu
	@PYTHON@ ./setup.py install --root=$(DESTDIR) --prefix=$(prefix) $(PXECONFIG_MAKE_EXTRA_ARGS)
	$(INSTALL) -d $(DESTDIR)$(SBIN_DIR)
	$(INSTALL) -d $(DESTDIR)$(BIN_DIR)
	$(INSTALL) -m 755 -o root src/pxeconfigd $(DESTDIR)$(SBIN_DIR)
	for script in $(PYTHON_FILES) ; \
	do \
	  $(INSTALL) -m 755 -o root src/$$script  $(DESTDIR)$(BIN_DIR)/$$script ;\
	done

install-conf:
	if [ ! -f  $(DESTDIR)/$(CONFIG)/pxeconfig.conf ] ; \
	then \
		$(INSTALL) -c -m 644 -o root pxeconfig.conf $(DESTDIR)/$(CONFIG)/pxeconfig.conf ;\
	fi

install-examples:
	$(INSTALL) -d $(DESTDIR)$(DATA_DIR)/examples
	for dir in $(EXAMPLES_DIR) ; \
	do \
		cp -a examples/$$dir $(DESTDIR)$(DATA_DIR)/examples/$$dir ;\
	done
	for file in $(EXAMPLES) ; \
	do \
	  $(INSTALL) -c -m 644 -o root examples/$$file  $(DESTDIR)$(DATA_DIR)/examples/$$file ;\
	done

install-pxemenu:
	$(INSTALL) -d $(DESTDIR)$(DATA_DIR)/examples
	for dir in $(PXEMENU_DIRS) ; \
	do \
		cp -a $$dir $(DESTDIR)$(DATA_DIR)/examples/bios/$$dir ;\
	done
	for file in $(PXEMENU_FILES) ; \
	do \
		$(INSTALL) -c -m 644 -o root $$file  $(DESTDIR)$(DATA_DIR)/examples/bios/$$file ;\
	done

clean: distclean

distclean:
	rm -f src/pxeconfig src/pxe_global.py src/pxeconfigd src/hexls examples/pxeconfigd.xinetd examples/pxeconfigd.inetd examples/pxeconfig@.service config.log config.status Makefile
	@PYTHON@ setup.py clean -a
