# PXE-utilities

This is a release of the SURF package of utilities that we use to boot
our nodes with PXE-enabled network cards. This was developed because we
always want to use the PXE-enabled network card as our first boot device.
The utilities work for `bios (pxelinux)` and `uefi` booting.  Depending on
the boot nethod a file will be placed in in `uefi` boot directory or the 
`bios` boot directory.

## Setup

In our setup we have a default setup. The default setup is to boot from
harddisk. When a node boots it fetches the default configuration file and the
configuration file tells the node to boot from the harddisk. This setup
is used for nodes that have already been installed by systemimager,[SALI](https://gitlab.com/surfsara/sali) or 
another bare metal installation tool.

At SURF we have developed the following  strategy how to install a fresh node:
>
> 1) The ip-address in our setup is known for the new node. So we make a link in the 
>  `/tftpboot` directory to bios/uefi config file which tells that it has to fetch 
>  the kernel and root filesystem from our bootserver, eg:
>     * 0A000A02          ---> default.node_install (10.0.10.2) -->  bios dir
>     * grub.cfg-0A000A02 ---> grub.cfg.node_install (10.0.10.2) --> uefi dir
>
>  2) The client starts and uses SALI to install  the node. The last line 
>   of installation script connects to a daemon on the bootserver. This daemon 
>   will remove the PXE config file for this node.
>
>  3) The node reboots and will use the default configuration file which will
>    instruct the node to boot from harddisk.
>

This release contains the following utilities. That are described in the following 
sections. They are all written in Python For more info about the Package and 
installation:
 * [pxeconfig wiki](https://gitlab.com/surfsara/pxeconfig/-/wikis/home)

## pxeconfigd:
this is daemon that removes the pxe configuration file of a node. We have xinetd and
systemd setup to start the pxeconfigd daemon

## pxeconfig

With this program we can specify which pxe config file
a node or nodes must use. This utility will create the links in `/tftpboot/pxelinux.cfg` 
or `/tftpboot/uefi`. This are the default directories can be overriden in the 
configuration file `pxeconfig.conf`

## hexls

Is a utility that diplays a directory, and, if in this directory
contains HEX files, it will convert them to IP-numbers.

##  BIOS pxe menu 

The PXE-menu tools are used to control the booting of networked computers
directly from the computer's console at the BIOS level before any operating
system has been loaded.  This is extremely useful for diagnostic purposes,
or for selecting an operating system installation method, of an individual
computer.  For further information please read the files README-pxemenu and
INSTALL-pxemenu. This files only work for BIOS mode booting.
